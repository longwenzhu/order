import './Header.less'
import { useContextData } from '/src/components/Context'
import fullImg from '/src/assets/full.svg'

export default function (props: { [key: string]: any }) {
  const {contextData, setContextData} = useContextData() as any
  const {langKey} = contextData
  let keyList = [{ name: '中文', key: 'name' }, { name: '日语', key: 'name_jp' }]

  //全屏操作
  function onFullScreen() {
    if (checkFull()) {
      closeFullScreen()

    } else {
      showFullScreen()
    }

  }
  function showFullScreen() {
    if (!document.fullscreenElement && !document.mozFullScreenElement
      && !document.webkitFullscreenElement && !document.msFullscreenElement) {
      if (document.documentElement.requestFullscreen) {
        document.documentElement.requestFullscreen();
      } else if (document.documentElement.msRequestFullscreen) {
        document.documentElement.msRequestFullscreen();
      } else if (document.documentElement.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen();
      } else if (document.documentElement.webkitRequestFullscreen) {
        document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
      }
    }
  }
  function closeFullScreen() {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    }
  }


  function checkFull() {
    var isFull = false;
    if (document.fullscreenEnabled || document.msFullscreenEnabled) {
      isFull = window.fullScreen || document.webkitIsFullScreen;
      if (isFull === undefined) {
        isFull = false;
      }
    }
    return isFull;
  }
  return (
    <div comp="header" className="header-box">
      <div className="left">
        {props.data[langKey]}
      </div>
      <div className="right">
        <div className="lang">
          {keyList.map((val, i) => {
            return (
              <span className={langKey == val.key ? 'active' : ''}
                onClick={() => setContextData({...contextData,langKey:val.key })}
                key={i}>{val.name}</span>
            )
          })}
        </div>
        <img onClick={onFullScreen} className="full-btn" src={fullImg} alt="" />
      </div>

    </div>
  )
}