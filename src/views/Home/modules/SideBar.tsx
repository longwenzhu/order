import { useState, useEffect } from 'react';
import { useContextData } from '/src/components/Context'

import './SideBar.less'
export default function (props: any) {
  const [curHash, setCurHash] = useState('')
  const { contextData,setContextData } = useContextData() as any
  const { langKey } = contextData

  useEffect(() => {
    setCurHash(window.location.hash.replace('#', ''))
    window.onhashchange = function (e) {
      setCurHash(window.location.hash.replace('#', ''))
    }
  }, [])

  return (
    <ul comp="sidebar">
      {
        props.data.map((item: any, i: number) => {
          return <li key={i}
            onClick={
              () => {
                setContextData({
                  ...contextData,
                  sideBarToggleIng:true
                })
              }
            }
            className={('good' + i === curHash || i === 0 && !curHash) ? 'active' : ''} >
            <a href={`#good${i}`}>{item[langKey]}</a>
          </li>
        })
      }
    </ul>
  )
}