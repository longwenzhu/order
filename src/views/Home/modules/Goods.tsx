import { useEffect, useRef,memo } from 'react';
import { useContextData } from '/src/components/Context'
import addImg from '/src/assets/add.png'
import './Goods.less'
export default memo(function (props: any) {
  const liRef = useRef<HTMLElement[]>([])
  liRef.current = []
  const {contextData, setContextData} = useContextData() as any
  const {langKey,sideBarToggleIng} = contextData

  function goodClick(e:any, item:any){
    setParabola(e)
    setContextData( (data:any) => {
      const list = JSON.parse(JSON.stringify(data.goodsList)) as any[]
      if(list.length == 0 ){
        list.push({...item, count:1})
      }else{
        let index
        list.some((val, i) => {
          if(val.name == item.name){
            index = i
            return true
          }
        })
        if(index == undefined){
          list.push({...item, count:1})
        }else{
          list[index].count++
        }
      }
      
      return {
        ...data,
        goodsList: list
      }
    })
  }
  function setParabola(e:any){
    //抛物线
    const el = e.target as HTMLElement,
    tar = document.querySelector('.total-up-box img')
    const clone = el.cloneNode(true) as HTMLElement
    el.parentElement?.appendChild(clone)
    var parabola = funParabola(clone,tar,{
      complete: function (){
        clone.remove()
      }
    } ).mark();
    parabola.init();
  }
  useEffect(() => {
    if (props.data.length) {
      const ul: HTMLElement = document.querySelector('[comp=goods]') as HTMLElement
      if(!sideBarToggleIng){
        ul.onscroll = function (e) {
          const resArr = liRef.current.filter(val => {
            if (val.getBoundingClientRect().y <= 70) {
              return val
            }
          })
          const ele = resArr[resArr.length - 1]
          window.location.hash = ele.getAttribute('id') || ''
  
        }
      }
      return () => {
        ul.onscroll = null
      }
      



      // const io = new IntersectionObserver(entries => {
      //   console.log(entries[0].isIntersecting, entries[0].target)

      // },{
      //   root: document.querySelector('[comp=goods]'),
      //   threshold:0.1,
      //   rootMargin:'0px 0px 99% 0px'
      // })

      // // Declares what to observe, and observes its properties.
      // liRef.current.forEach((el) => {
      //   io.observe(el);
      // })


    }


  }, [props.data, langKey,sideBarToggleIng])
  

  return (
    <ul comp="goods" onTouchStart={() => {
      setContextData({
        ...contextData,
        sideBarToggleIng:false
      })
    }}>
      {props.data[0]?.children?.length
        &&
        props.data.map((item, i) => {
          let goodItem = item.children.map((item2: any) => {
            return <div className='item' key={item2.name}>
              <img className='poster' src={item2.photo} alt="" />
              <h3>{item2[langKey]}</h3>
              <div className='option'>
                <span className='price'>{item2.price}</span>
                <img onClick={(e:any) => goodClick(e, item2)}  
                src={addImg} className='add-btn' alt="" />
              </div>
            </div>
          })
          return (
            <li ref={
              (val: HTMLElement) => {
                if (val) {
                  liRef.current.push(val)
                } else {
                  liRef.current.splice(i, 1)
                }
              }
            } key={i} id={'good' + i}>
              {goodItem}
            </li>
          )
        })
      }
    </ul>
  )


}) 