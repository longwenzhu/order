import React, { Children, useCallback, useEffect, useMemo, useState } from "react"
import Header from "./modules/Header"
import SideBar from "./modules/SideBar"
import Goods from './modules/Goods'
import ShoppingCar from './modules/ShoppingCar'

export default function () {

  const [list, setList] = useState([])
  const [info, setInfo] = useState({})


  useEffect(() => {
    fetch('http://manage.lilll.cn/api/merchant_menu/app/list?view_code=gsyERt09Uw')
      .then(res => res.json())
      .then(res => {
        setList(res.data.list)
        setInfo(res.data.info)
      })
  }, [])


  const style = {
    display: "flex",
    height: 'calc(100vh - 60px)',
  }


  return (
    <div>
      <Header data={info} />
      <div style={style}>
        <SideBar data={list} />
        <Goods data={list} ></Goods>
      </div>
      <ShoppingCar />

    </div>
  )
}


