import { createContext, useContext, useState } from "react"
const data = {
  langKey:'name',
  goodsList:[],
  sideBarToggleIng: false
}
const context = createContext({})
export function Provider(props:any){
  const [contextData, setContextData] = useState(data)
  return <context.Provider value={{contextData, setContextData}}>
    {props.children}
  </context.Provider>
}
export function useContextData(){
  return useContext(context)
}